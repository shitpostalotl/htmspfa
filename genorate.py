#! /bin/python
import json, copy

# read template and story data
with open("template.html", "r") as templatefile: template = templatefile.read()
with open("data.json", "r") as datafile: storydata = json.load(datafile)

# debug print inputs
print(template+"\n"+json.dumps(storydata, indent=4))

# replace template data with story data
for i in range(len(storydata)):
	page = copy.copy(template)
	while "$story" in page: page = page.replace("$story",storydata[0]["title"])
	page = page.replace("$title",storydata[i]["title"])
	page = page.replace("$pic",str(i))
	page = page.replace("$alt",storydata[i]["alt"])
	page = page.replace("$nar",storydata[i]["nar"])
	page = page.replace("$logtype",storydata[i]["logtype"])
	page = page.replace("$loghtml",storydata[i]["loghtml"])
	try: page = page.replace("$nexttitle",storydata[i+1]["title"])
	except: page = page.replace("$nexttitle","")
	page = page.replace("$pagenumber+1",(str(i+1)+".html")*(i<len(storydata)))
	page = page.replace("$pagenumber-1",(str(i-1)+".html")*(i>0))

	# output file
	with open(str(i)+'.html', 'w') as outputfile: outputfile.write(page)
